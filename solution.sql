--a
SELECT * FROM artists WHERE name LIKE "%d%";

--b
SELECT * FROM songs WHERE length < 350;

--c
SELECT albums.album_title, songs.song_name, songs.length 
FROM songs JOIN albums
ON songs.album_id = albums.id;

--d
SELECT *
FROM artists JOIN albums
ON artists.id = albums.artist_id
WHERE album_title LIKE "%a%";

--e
SELECT * FROM albums
ORDER BY album_title DESC
LIMIT 4;

--f
SELECT *
FROM songs JOIN albums
ON songs.album_id = albums.id
ORDER BY album_title DESC;